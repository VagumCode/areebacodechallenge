package com.areeba.code.challenge.db.entities;

import com.areeba.code.challenge.db.manager.DBEntity;

import javax.persistence.*;
import java.util.Date;

@Entity
public class ActionLog extends DBEntity<ActionLog> {

    @Id
    @GeneratedValue
    private int actionLogId;
    @ManyToOne
    private Device device;
    @OneToOne
    private Action action;
    private Date date;

    public static final String FLD_ID    = "actionLogId";

    public ActionLog() {
        super(ActionLog.class);
    }

    public void setActionLogId(int actionLogId) {
        this.actionLogId = actionLogId;
    }

    public int getActionLogId() {
        return actionLogId;
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}

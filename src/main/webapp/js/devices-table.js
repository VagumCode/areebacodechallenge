var onRemoveDeviceClickListener = function (e, removeDeviceUrl, deviceId) {
    try {
        jQuery.ajax({
            url: removeDeviceUrl,
            type: "DELETE",
            complete: function () {
                $("#device-row-" + deviceId).remove();
            },
            success: function (response) {},
            error: function (jqXHR, errorMessage, errorCode) {}
        });
    } catch (e) {
        debug("onRemoveDeviceClickListener", e);
    }
};
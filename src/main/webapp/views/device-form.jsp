<%@ page import="java.util.Date" %><%--
  Created by IntelliJ IDEA.
  User: Hadi 
  Date: 11/10/19
  Time: 3:05 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="th" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.1.min.js"></script>
        <script src="../js/utils.js"></script>
        <script src="../js/device-form.js"></script>
        <title>Device</title>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">Device</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#registered-actions-nav-bar" aria-controls="registered-actions-nav-bar" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="registered-actions-nav-bar">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="/CodeChallenge_war_exploded">Home <span class="sr-only">(current)</span></a>
                    </li>
                </ul>
            </div>
        </nav>
        <form id="edit-device-form" >
            <div class="row">
                <div class="col-xl">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="device-number">Number</span>
                        </div>
                        <input value="${selectedDevice.number}" type="text" class="form-control" aria-label="Device Number" name="deviceNumber" aria-describedby="device-number" />
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="device-display-name">Display Name</span>
                        </div>
                        <input value="${selectedDevice.displayName}" type="text" class="form-control" aria-label="Display Name" name="deviceDisplayName" aria-describedby="device-display-name" />
                    </div>
                </div>
            </div>
        </form>

        <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active" id="nav-actions-tab" data-toggle="tab" href="#nav-add-actions" role="tab" aria-controls="nav-add-actions" aria-selected="true">Add Actions</a>
                <a class="nav-item nav-link" id="nav-logs-tab" data-toggle="tab" href="#nav-actions-logs" role="tab" aria-controls="nav-actions-logs" aria-selected="false">Action Logs</a>
            </div>
        </nav>
        <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="nav-add-actions" role="tabpanel" aria-labelledby="nav-actions-tab">
                <%long dateTimeInMilliseconds = new Date().getTime();%>
                <div class="d-flex flex-column" style="margin: 25px;width: 20%;">
                    <c:forEach var="currentAction" items="${actionsList}">
                        <button style="margin-bottom: 10px;" type="button" class="btn btn-secondary" onclick="onActionLogClickListener(event, ${selectedDevice.deviceId}, ${currentAction.actionId}, <%=dateTimeInMilliseconds%>)">${currentAction.actionTitle}</button>
                    </c:forEach>
                </div>
            </div>
            <div class="tab-pane fade" id="nav-actions-logs" role="tabpanel" aria-labelledby="nav-logs-tab">
                <form id="action-logs-form">
                    <table class="table table-hover" style="margin: 25px;">
                        <thead class="thead-light">
                        <tr>
                            <th id="action-name" scope="col">Action Name</th>
                            <th id="action-date-time" scope="col">Date/Time</th>
                        </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="currentActionsLogs" items="${actionsLogsList}">
                                <tr>
                                    <td headers="action-name">${currentActionsLogs.action.actionTitle}</td>
                                    <td headers="action-date-time">${currentActionsLogs.date}</td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
    </body>
</html>

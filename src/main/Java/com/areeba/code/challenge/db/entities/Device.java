package com.areeba.code.challenge.db.entities;

import com.areeba.code.challenge.db.manager.DBEntity;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Device extends DBEntity<Device> {

    @Id
    @GeneratedValue
    private int deviceId;
    private String number;
    private String displayName;
    @OneToMany(targetEntity = ActionLog.class, mappedBy = "device", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<ActionLog> actionsLogs;

    public static final String FLD_NUMBER = "number";
    public static final String FLD_DEVICE_ID = "deviceId";

    public Device() {
        super(Device.class);
    }

    public int getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(int deviceId) {
        this.deviceId = deviceId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Set<ActionLog> getActionsLogs() {
        return actionsLogs;
    }

    @Override
    public String getOrderPropertyId() {
        return FLD_DEVICE_ID;
    }
}

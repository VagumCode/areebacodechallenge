package com.areeba.code.challenge.db.manager;

import com.areeba.code.challenge.utils.logger.JLogger;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.engine.spi.SessionFactoryDelegatingImpl;

import javax.persistence.*;
import javax.persistence.criteria.*;
import java.util.List;

public class DBServices<E extends DBEntity> {

    private Class<E> entityClass = null;

    public DBServices(Class<E> entityClass) {
        this.entityClass = entityClass;
    }

    public void dispose() {

    }

    public void insert(E dbEntity) {
        Session session = DBConfiguration.getInstance().openSession();
        try {
            Transaction transaction = session.beginTransaction();
            session.save(dbEntity);
            transaction.commit();
        } catch (Exception e) {
            JLogger.getLogger().logException(e);
        } finally {
            session.close();
        }
    }

    private Session openSession() {
        return DBConfiguration.getInstance().openSession();
    }

    public List<E> selectAll() {
        return selectAll(null);
    }

    public List<E> selectAll(Order orderBy) {
        Session          session              = openSession();
        List<E> selectResult = null;
        try {
            CriteriaBuilder criteriaBuilder       = session.getCriteriaBuilder();
            CriteriaQuery<E> eCreateCriteriaQuery = criteriaBuilder.createQuery(getEntityClass());
            Root<E>          eRootEntry           = eCreateCriteriaQuery.from(getEntityClass());
            CriteriaQuery<E> eSelectCriteriaQuery = eCreateCriteriaQuery.select(eRootEntry);
            if (orderBy != null) {
                Path orderByPath = eRootEntry.get(orderBy.getPropertyName());
                javax.persistence.criteria.Order order = null;
                if (orderBy.isAscending()) {
                    order = criteriaBuilder.asc(orderByPath);
                } else {
                    order = criteriaBuilder.desc(orderByPath);
                }
                eSelectCriteriaQuery.orderBy(order);
            }
            TypedQuery<E>    eTypedQuery          = session.createQuery(eSelectCriteriaQuery);
            selectResult = eTypedQuery.getResultList();
        } catch (Exception e) {
            JLogger.getLogger().logException(e);
        } finally {
            session.close();
        }
        return selectResult;
    }

    public E find(String propertyId, Object propertyValue) {
        Session          session         = openSession();
        E result = null;
        try {
            CriteriaBuilder  criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<E> eCriteriaQuery  = criteriaBuilder.createQuery(getEntityClass());
            Root<E>          rootFromEntry   = eCriteriaQuery.from(getEntityClass());
            eCriteriaQuery.where(criteriaBuilder.equal(rootFromEntry.get(propertyId), propertyValue));
            CriteriaQuery<E> eSelectCriteriaQuery = eCriteriaQuery.select(rootFromEntry);
            TypedQuery<E>    eTypedQuery          = session.createQuery(eSelectCriteriaQuery);
            result = eTypedQuery.getSingleResult();
        } catch (Exception e) {
            JLogger.getLogger().logException(e);
        } finally {
            session.close();
        }
        return result;
    }

    public void remove(String propertyId, Object propertyValue) {
        Session          session         = openSession();
        try {
            Transaction transaction = session.beginTransaction();
            session.createQuery("DELETE  FROM " + getEntityClass().getSimpleName() + " where " + propertyId + "='" + propertyValue + "'").executeUpdate();
            transaction.commit();
        } catch (Exception e) {
            JLogger.getLogger().logException(e);
        } finally {
            session.close();
        }
    }

    private Class<E> getEntityClass() {
        return entityClass;
    }
}

package com.areeba.code.challenge.controller.device;

import com.areeba.code.challenge.service.device.DeviceService;

public class Controller_DeviceCommon {

    protected static final String KEY_DEVICE_DISPLAY_NAME = "deviceDisplayName";
    protected static final String KEY_DEVICE_NUMBER       = "deviceNumber";

    protected static final String ATT_REGISTERED_DEVICES_LIST = "registeredDevicesList";
    protected static final String ATT_SELECTED_DEVICE = "selectedDevice";
    public static final String ATT_ACTIONS_LOGS = "actionsLogsList";
    protected static final String ATT_ACTIONS = "actionsList";

    protected static final String GET_MAP_SHOW_ALL_DEVICES = "showAll";

    protected static final String POST_MAP_ADD_NEW_DEVICE = "/register";
    protected static final String POST_MAP_REMOVE_DEVICE = "/remove";

    private DeviceService deviceService = null;

    public void dispose() {
        if (deviceService != null) {
            deviceService.dispose();
            deviceService = null;
        }
    }

    protected DeviceService getDeviceService() {
        if (deviceService == null) {
            deviceService = new DeviceService();
        }
        return deviceService;
    }
}

package com.areeba.code.challenge.service.device;

import com.areeba.code.challenge.db.entities.Device;
import com.areeba.code.challenge.utils.JUtilities;
import com.areeba.code.challenge.utils.logger.JLogger;
import org.hibernate.criterion.Order;

import java.util.List;

public class DeviceService {

    public void dispose() {
    }

    public void newDevice(String deviceDisplayName, String deviceNumber) {
        try {
            Device foundDevice = findDeviceByNumber(deviceNumber);
            if (foundDevice == null) {
                Device device = new Device();
                device.setNumber(deviceNumber);
                device.setDisplayName(deviceDisplayName);
                device.insert();
            }
        } catch (Exception e) {
            JLogger.getLogger().logException(e);
        }
    }

    public void removeDevice(String deviceNumber) {
        try {
            Device deviceEntity = new Device();
            deviceEntity.remove(Device.FLD_NUMBER, deviceNumber);
            deviceEntity.dispose();
            deviceEntity = null;
        } catch (Exception e) {
            JLogger.getLogger().logException(e);
        }
    }

    public Device findDeviceByNumber(String deviceNumber) {
        Device device = null;
        try {
            Device deviceEntity = new Device();
            device = deviceEntity.find(Device.FLD_NUMBER, deviceNumber);
            deviceEntity.dispose();
            deviceEntity = null;
        } catch (Exception e) {
            JLogger.getLogger().logException(e);
        }
        return device;
    }

    public Device findDeviceById(int deviceId) {
        Device device = null;
        try {
            Device deviceEntity = new Device();
            device = deviceEntity.find(Device.FLD_DEVICE_ID, deviceId);
            deviceEntity.dispose();
            deviceEntity = null;
        } catch (Exception e) {
            JLogger.getLogger().logException(e);
        }
        return device;
    }

    public List<Device> getDevicesList() {
        return getDevicesList(false);
    }

    public List<Device> getDevicesList(boolean inAscendingOrder) {
        try {
            Device device = new Device();
            Order orderBy = null;
            if (JUtilities.isStringValid(device.getOrderPropertyId())) {
                orderBy = inAscendingOrder ? Order.asc(device.getOrderPropertyId()) : Order.desc(device.getOrderPropertyId());
            }
            List<Device> devicesList = device.selectAll(orderBy);
            device.dispose();
            device = null;
            return devicesList;
        } catch (Exception e) {
            JLogger.getLogger().logException(e);
        }
        return null;
    }
}

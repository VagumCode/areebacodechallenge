package com.areeba.code.challenge.utils;

import com.areeba.code.challenge.utils.logger.JLogger;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class DateUtilities {

	private static Map<String, String> DATE_FORMAT_REGEXPS = new HashMap<String, String>(){
		/**
		 * 
		 */
		private static final long serialVersionUID = -7496790385268643085L;

	{
		 put("^\\d{8}$", "yyyyMMdd");
		 put("^\\d{1,2}-\\d{1,2}-\\d{4}$", "dd-MM-yyyy");
		 put("^\\d{4}-\\d{1,2}-\\d{1,2}$", "yyyy-MM-dd");
		 put("^\\d{1,2}/\\d{1,2}/\\d{4}$", "MM/dd/yyyy");
		 put("^\\d{4}/\\d{1,2}/\\d{1,2}$", "yyyy/MM/dd");
		 put("^\\d{1,2}\\s[a-z]{3}\\s\\d{4}$", "dd MMM yyyy");
		 put("^\\d{1,2}-\\s[a-z]{3}-\\d{4}$", "dd-MMM-yyyy");
		 put("^\\d{1,2}\\s[a-z]{4,}\\s\\d{4}$", "dd MMMM yyyy");
		 put("^\\d{12}$", "yyyyMMddHHmm");
		 put("^\\d{8}\\s\\d{4}$", "yyyyMMdd HHmm");
		 put("^\\d{1,2}-\\d{1,2}-\\d{4}\\s\\d{1,2}:\\d{2}$", "dd-MM-yyyy HH:mm");
		 put("^\\d{4}-\\d{1,2}-\\d{1,2}\\s\\d{1,2}:\\d{2}$", "yyyy-MM-dd HH:mm");
		 put("^\\d{1,2}/\\d{1,2}/\\d{4}\\s\\d{1,2}:\\d{2}$", "MM/dd/yyyy HH:mm");
		 put("^\\d{4}/\\d{1,2}/\\d{1,2}\\s\\d{1,2}:\\d{2}$", "yyyy/MM/dd HH:mm");
		 put("^\\d{1,2}\\s[a-z]{3}\\s\\d{4}\\s\\d{1,2}:\\d{2}$", "dd MMM yyyy HH:mm");
		 put("^\\d{1,2}\\s[a-z]{4,}\\s\\d{4}\\s\\d{1,2}:\\d{2}$", "dd MMMM yyyy HH:mm");
		 put("^\\d{14}$", "yyyyMMddHHmmss");
		 put("^\\d{8}\\s\\d{6}$", "yyyyMMdd HHmmss");
		 put("^\\d{1,2}-\\d{1,2}-\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}$", "dd-MM-yyyy HH:mm:ss");
		 put("^\\d{4}-\\d{1,2}-\\d{1,2}\\s\\d{1,2}:\\d{2}:\\d{2}$", "yyyy-MM-dd HH:mm:ss");
		 put("^\\d{1,2}/\\d{1,2}/\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}$", "MM/dd/yyyy HH:mm:ss");
		 put("^\\d{4}/\\d{1,2}/\\d{1,2}\\s\\d{1,2}:\\d{2}:\\d{2}$", "yyyy/MM/dd HH:mm:ss");
		 put("^\\d{1,2}\\s[a-z]{3}\\s\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}$", "dd MMM yyyy HH:mm:ss");
		 put("^\\d{1,2}\\s[a-z]{4,}\\s\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}$", "dd MMMM yyyy HH:mm:ss");
	}};
	
	private static Date extraDateFormat(String dateValue) {
		Date date = dateParser("EEEE MMM dd HH:mm:ss zzzz yyyy", dateValue);
		if(date == null) {
			date = dateParser("dd-MMM-yyyy", dateValue);
		}
		if(date == null) {
			date = dateParser("E MMM dd hh:mm:ss Z yyyy", dateValue);//Wed Nov 08 12:12:36 CST 2017
		}
		//This means the current format isn't exists.
		if(date == null) {
			JLogger.getLogger().logExceptionInfo(dateValue);
		}
		return date;
	}
	
	private static Date dateParser(String dateFormat, String dateValue) {
		Date date = null;
		try {
			SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
		    date = formatter.parse(dateValue);
		}catch (Exception e) {
			date = null;
		}
		return date;
	}
	
	private static String findDateFormat(String dateString) {
	    for (String regexp : DATE_FORMAT_REGEXPS.keySet()) {
	        if (dateString.toLowerCase().matches(regexp)) {
	            return DATE_FORMAT_REGEXPS.get(regexp);
	        }
	    }
	    return null; // Unknown format.
	}
	
	public static java.sql.Date toMySqlDate(Object dateValue){
		java.sql.Date mySqlDate = null;
		try{
			if(JUtilities.isStringValid(dateValue+"")){
				 String dateFormat = findDateFormat(String.valueOf(dateValue));
				 if(dateFormat != null && !dateFormat.isEmpty()){
					 SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
					 Date date = simpleDateFormat.parse(String.valueOf(dateValue));
					 mySqlDate = new java.sql.Date(date.getTime());
				 }else {
					 try {
						 mySqlDate = new java.sql.Date(extraDateFormat(String.valueOf(dateValue)).getTime());
					 }catch (Exception e) {
						 JLogger.getLogger().logException(null, "Date format '" + dateValue + "' not found!");
					 }
				 }
			}
		}catch (Exception e) {
			JLogger.getLogger().logException(e);
		}
		return mySqlDate;
	}

	public static String getLebaneseDateTime() {
		LocalDateTime beirutLebanonLocalDateTime = LocalDateTime.now(ZoneId.of("Asia/Beirut"));
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy HH:mm:ss");
		String formattedBeirutLebanonLocalDateTime = beirutLebanonLocalDateTime.format(dateTimeFormatter);
		return formattedBeirutLebanonLocalDateTime;
	}

	public static String getLebaneseDate() {
		LocalDateTime beirutLebanonLocalDateTime = LocalDateTime.now(ZoneId.of("Asia/Beirut"));
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy");
		String formattedBeirutLebanonLocalDateTime = beirutLebanonLocalDateTime.format(dateTimeFormatter);
		return formattedBeirutLebanonLocalDateTime;
	}

	public long getGreenwichDateTime() {
		return Calendar.getInstance(TimeZone.getTimeZone("GMT")).getTimeInMillis();
	}
}

package com.areeba.code.challenge.controller.action;

import com.areeba.code.challenge.service.action.ActionService;

public class Controller_ActionCommon {

    protected static final String POST_MAP_ADD_NEW_ACTION = "/addAction";
    protected static final String ATT_REGISTERED_ACTIONS_LIST = "registeredActionsList";

    private ActionService actionService = null;

    public void dispose() {
        if (actionService != null) {
            actionService.dispose();
            actionService = null;
        }
    }

    protected ActionService getActionService() {
        if (actionService == null) {
            actionService = new ActionService();
        }
        return actionService;
    }
}

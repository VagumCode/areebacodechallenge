<%--
  Created by IntelliJ IDEA.
  User: Hadi 
  Date: 11/10/19
  Time: 9:43 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="th" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
    <head>
        <title>Actions</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.1.min.js"></script>

        <script src="../js/utils.js"></script>
        <script src="../js/actions-table.js"></script>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">Registered Actions</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#registered-actions-nav-bar" aria-controls="registered-actions-nav-bar" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="registered-actions-nav-bar">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="/CodeChallenge_war_exploded">Home <span class="sr-only">(current)</span></a>
                    </li>
                </ul>
            </div>
        </nav>

        <table class="table table-hover">
            <thead class="thead-light">
                <tr>
                    <th id="action-title" scope="col">Action Title</th>
                    <th id="actions" scope="col">Actions</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="currentAction" items="${registeredActionsList}">
                    <tr id="action-row-${currentAction.actionId}">
                        <td headers="action-title" style="padding-bottom: 0px; padding-top: 0px;">
                            <button class="btn btn-link">${currentAction.actionTitle}</button>
                        </td>
                        <td headers="actions" style="padding-bottom: 0px; padding-top: 0px;">
                            <spring:url value="../action/delete/${currentAction.actionId}" var="removeActionUrl"></spring:url>
                            <button class="btn btn-link" onclick="onRemoveActionClickListener(event, '${removeActionUrl}', ${currentAction.actionId})">Remove</button>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </body>
</html>

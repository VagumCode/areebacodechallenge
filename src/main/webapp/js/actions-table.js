var onRemoveActionClickListener = function (e, removeActionUrl, actionId) {
    try {
        jQuery.ajax({
            url: removeActionUrl,
            type: "DELETE",
            complete: function () {
                $("#action-row-" + actionId).remove();
            },
            success: function (response) {},
            error: function (jqXHR, errorMessage, errorCode) {}
        });
    } catch (e) {
        debug("onRemoveDeviceClickListener", e);
    }
};
package com.areeba.code.challenge.controller.device;

import com.areeba.code.challenge.config.AppConfiguration;
import com.areeba.code.challenge.utils.logger.JLogger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/registeredDevices")
public class Controller_DevicesTable extends Controller_DeviceCommon {

    /**
     * ex: http://localhost:8080/CodeChallenge_war_exploded/registeredDevices/showAll
     * @return
     */
    @GetMapping(GET_MAP_SHOW_ALL_DEVICES)
    public ModelAndView getDevicesList() {
        ModelAndView modelAndView = new ModelAndView();
        try {
            modelAndView.setViewName(AppConfiguration.PAGE_DEVICES_TABLE);
            modelAndView.addObject(ATT_REGISTERED_DEVICES_LIST, getDeviceService().getDevicesList(false));
        } catch (Exception e) {
            JLogger.getLogger().logException(e);
        }
        return modelAndView;
    }
}

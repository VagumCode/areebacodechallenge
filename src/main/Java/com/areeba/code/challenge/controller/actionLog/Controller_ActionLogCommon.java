package com.areeba.code.challenge.controller.actionLog;

import com.areeba.code.challenge.service.actionLog.ActionLogService;

public class Controller_ActionLogCommon {

    private ActionLogService actionLogService = null;

    protected ActionLogService getActionLogService() {
        if (actionLogService == null) {
            actionLogService = new ActionLogService();
        }
        return actionLogService;
    }
}

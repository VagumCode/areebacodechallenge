<html lang="en">
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <title>Code Challenge</title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-xl"></div>
                <div class="col-xl">
                    <p class="font-weight-bold text-center">Areeba Code Challenge</p>
                </div>
                <div class="col-xl"></div>
            </div>
            <div class="row">
                <div class="col-xl"></div>
                <div class="col-xl">
                    <p class="font-weight-bold text-center">Hadi Abou Hamzeh</p>
                </div>
                <div class="col-xl"></div>
            </div>
            <div class="row justify-content-center">
                <div class="col-xl"></div>
                <div class="col-xl" style="margin-left: 50%; margin-right: 50%;">
                    <div class="btn-group" role="group" aria-label="Devices Manager">
                        <button onclick="javascript:location.href='registeredDevices/showAll'" type="button" class="btn btn-secondary" role="button">View Devices</button>
                        <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#register-device-dialog">Register Device</button>
                    </div>
                    <div class="btn-group" role="group" aria-label="Devices Manager" style="margin-top: 10px;width: 100%;">
                        <button onclick="javascript:location.href='action/showAll'" type="button" class="btn btn-secondary" role="button">View Actions</button>
                        <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#add-action-dialog" style="width: 100%;">Create Action</button>
                    </div>
                </div>
                <div class="col-xl"></div>
            </div>
        </div>

        <!-- Device Modal Start -->
        <div class="modal fade" id="register-device-dialog" tabindex="-1" role="dialog" aria-labelledby="add-new-device-title" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="add-new-device-title">Add New Device</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form id="add-new-device-form" action="device/register" method="post">
                            <div class="row">
                                <div class="col-xl">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="device-number">Number</span>
                                        </div>
                                        <input type="text" class="form-control" aria-label="Device Number" name="deviceNumber" aria-describedby="device-number" value="">
                                    </div>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="device-display-name">Display Name</span>
                                        </div>
                                        <input type="text" class="form-control" aria-label="Display Name" name="deviceDisplayName" aria-describedby="device-display-name" value="">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" form="add-new-device-form" class="btn btn-primary">Add Device</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Device Modal End -->

        <!-- Actions Modal Start -->
        <div class="modal fade" id="add-action-dialog" tabindex="-1" role="dialog" aria-labelledby="add-new-action-dialog-title" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="add-new-action-dialog-title">Add New Device Action</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form id="add-new-action-form" action="action/addAction" method="post">
                            <div class="row">
                                <div class="col-xl">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="action-title">Title</span>
                                        </div>
                                        <input type="text" class="form-control" aria-label="Action Title" name="actionTitle" aria-describedby="action-title" value="">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" form="add-new-action-form" class="btn btn-primary">Add Action</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Actions Modal End -->
    </body>
</html>

package com.areeba.code.challenge.service.action;

import com.areeba.code.challenge.db.entities.Action;
import com.areeba.code.challenge.utils.JUtilities;
import com.areeba.code.challenge.utils.logger.JLogger;
import org.hibernate.criterion.Order;

import java.util.List;

public class ActionService {

    public void dispose() {
    }

    public void newAction(String title) {
        try {
            Action foundAction = findActionByTitle(title);
            if (foundAction == null) {
                Action action = new Action();
                action.setActionTitle(title);
                action.insert();
            }
        } catch (Exception e) {
            JLogger.getLogger().logException(e);
        }
    }

    public void removeAction(String actionTitle) {
        try {
            Action actionEntity = new Action();
            actionEntity.remove(Action.FLD_TITLE, actionTitle);
            actionEntity.dispose();
            actionEntity = null;
        } catch (Exception e) {
            JLogger.getLogger().logException(e);
        }
    }

    public void removeAction(int actionId) {
        try {
            Action actionEntity = new Action();
            actionEntity.remove(Action.FLD_ID, actionId);
            actionEntity.dispose();
            actionEntity = null;
        } catch (Exception e) {
            JLogger.getLogger().logException(e);
        }
    }

    public Action findActionByTitle(String actionTitle) {
        Action action = null;
        try {
            Action actionEntity = new Action();
            action = actionEntity.find(Action.FLD_TITLE, actionTitle);
            actionEntity.dispose();
            actionEntity = null;
        } catch (Exception e) {
            JLogger.getLogger().logException(e);
        }
        return action;
    }

    public Action findActionById(int actionId) {
        Action action = null;
        try {
            Action actionEntity = new Action();
            action = actionEntity.find(Action.FLD_ID, actionId);
            actionEntity.dispose();
            actionEntity = null;
        } catch (Exception e) {
            JLogger.getLogger().logException(e);
        }
        return action;
    }

    public List<Action> getActionsList() {
        return getActionsList(false);
    }

    public List<Action> getActionsList(boolean inAscendingOrder) {
        try {
            Action action = new Action();
            Order orderBy = null;
            if (JUtilities.isStringValid(action.getOrderPropertyId())) {
                orderBy = inAscendingOrder ? Order.asc(action.getOrderPropertyId()) : Order.desc(action.getOrderPropertyId());
            }
            List<Action> actionsList = action.selectAll(orderBy);
            action.dispose();
            action = null;
            return actionsList;
        } catch (Exception e) {
            JLogger.getLogger().logException(e);
        }
        return null;
    }
}

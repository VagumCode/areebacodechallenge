package com.areeba.code.challenge.controller.actionLog;

import com.areeba.code.challenge.config.AppConfiguration;
import com.areeba.code.challenge.controller.device.Controller_DeviceCommon;
import com.areeba.code.challenge.db.entities.Action;
import com.areeba.code.challenge.db.entities.ActionLog;
import com.areeba.code.challenge.db.entities.Device;
import com.areeba.code.challenge.service.action.ActionService;
import com.areeba.code.challenge.service.device.DeviceService;
import com.areeba.code.challenge.utils.logger.JLogger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/actionsLog")
public class Controller_ActionLog extends Controller_ActionLogCommon {

    @PostMapping("/logAction/{deviceId}/{currentDateTimeLong}/{actionId}")
    public ModelAndView logAction(@PathVariable int deviceId, @PathVariable long currentDateTimeLong, @PathVariable int actionId) {
        ModelAndView modelAndView = new ModelAndView();
        try {
            DeviceService deviceService = new DeviceService();
            Device device = deviceService.findDeviceById(deviceId);

            ActionService actionService = new ActionService();
            Action action = actionService.findActionById(actionId);

            getActionLogService().newActionLog(device, action, new Date(currentDateTimeLong));

            deviceService.dispose();
            deviceService = null;

            actionService.dispose();
            actionService = null;

            modelAndView.setViewName(AppConfiguration.PAGE_DEVICE_FORM);
            modelAndView.addObject(Controller_DeviceCommon.ATT_ACTIONS_LOGS, actionService.getActionsList(false));
        } catch (Exception e) {
            JLogger.getLogger().logException(e);
        }
        return modelAndView;
    }

    @GetMapping("/actionsLogs/{deviceId}")
    public List<ActionLog> getActionsLogsList(@PathVariable int deviceId) {
        List<ActionLog> actionsLogsList = null;
        try {
            DeviceService deviceService = new DeviceService();
            Device device = deviceService.findDeviceById(deviceId);
            if (device != null) {
                actionsLogsList = new ArrayList<>(device.getActionsLogs());
            }
        } catch (Exception e) {
            JLogger.getLogger().logException(e);
        }
        return actionsLogsList;
    }

}

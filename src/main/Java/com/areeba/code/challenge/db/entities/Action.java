package com.areeba.code.challenge.db.entities;

import com.areeba.code.challenge.db.manager.DBEntity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Action extends DBEntity<Action> {

    public static final String FLD_ID    = "actionId";
    public static final String FLD_TITLE = "actionTitle";

    @Id
    @GeneratedValue
    private int actionId;
    private String actionTitle;

    public Action() {
        super(Action.class);
    }

    public int getActionId() {
        return actionId;
    }

    public void setActionId(int actionId) {
        this.actionId = actionId;
    }

    public String getActionTitle() {
        return actionTitle;
    }

    public void setActionTitle(String actionTitle) {
        this.actionTitle = actionTitle;
    }
}

package com.areeba.code.challenge.controller.device;

import com.areeba.code.challenge.config.AppConfiguration;
import com.areeba.code.challenge.db.entities.Device;
import com.areeba.code.challenge.service.action.ActionService;
import com.areeba.code.challenge.utils.logger.JLogger;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

//https://youtu.be/g2b-NbR48Jo
//https://www.baeldung.com/spring-controllers
@Controller
@RequestMapping(value = "/device")
public class Controller_Device extends Controller_DeviceCommon {

    @PostMapping(POST_MAP_ADD_NEW_DEVICE)
    public @ResponseBody ModelAndView register(@RequestParam(KEY_DEVICE_DISPLAY_NAME) String deviceDisplayName, @RequestParam(KEY_DEVICE_NUMBER) String deviceNumber, SessionStatus sessionStatus) {
        ModelAndView modelAndView = new ModelAndView();
        try {
            Device device = getDeviceService().findDeviceByNumber(deviceNumber);
            if (device == null) {
                getDeviceService().newDevice(deviceDisplayName, deviceNumber);
                modelAndView.addObject(ATT_REGISTERED_DEVICES_LIST, getDeviceService().getDevicesList(false));
                modelAndView.setStatus(HttpStatus.OK);
            } else {
                modelAndView.addObject("message", "");
            }
            modelAndView.setViewName(AppConfiguration.PAGE_DEVICES_TABLE);
            sessionStatus.setComplete();
        } catch (Exception e) {
            JLogger.getLogger().logException(e);
        }
        return modelAndView;
    }

    @DeleteMapping("/delete/{" + KEY_DEVICE_NUMBER + "}")
    @ResponseStatus(HttpStatus.OK)
    public void removeDevice(@PathVariable String deviceNumber) {
        try {
            getDeviceService().removeDevice(deviceNumber);
        } catch (Exception e) {
            JLogger.getLogger().logException(e);
        }
    }

    @GetMapping("/{" + KEY_DEVICE_NUMBER + "}")
    public ModelAndView findDeviceByNumber(@PathVariable String deviceNumber) {
        ModelAndView modelAndView = new ModelAndView();
        try {
            Device device = getDeviceService().findDeviceByNumber(deviceNumber);
            if (device != null) {
                ActionService actionService = new ActionService();
                modelAndView.addObject(ATT_SELECTED_DEVICE, getDeviceService().findDeviceByNumber(deviceNumber));
                modelAndView.addObject(ATT_ACTIONS, actionService.getActionsList(false));
                modelAndView.addObject(ATT_ACTIONS_LOGS, device.getActionsLogs());
                actionService.dispose();
                actionService = null;
            }
            modelAndView.setViewName(AppConfiguration.PAGE_DEVICE_FORM);
            device.dispose();
            device = null;
        } catch (Exception e) {
            JLogger.getLogger().logException(e);
        }
        return modelAndView;
    }
}

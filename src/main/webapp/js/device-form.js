var onActionLogClickListener = function (e, deviceId, actionId, dateTimeInMilliseconds) {
    try {
        jQuery.ajax({
            url: "../actionsLog/logAction/" + deviceId + "/" + dateTimeInMilliseconds + "/" + actionId,
            type: "POST",
            complete: function () {},
            success: function (response) {},
            error: function (jqXHR, errorMessage, errorCode) {}
        });
    } catch (e) {
        debug("onActionLogClickListener", e);
    }
};
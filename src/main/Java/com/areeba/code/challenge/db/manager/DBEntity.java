package com.areeba.code.challenge.db.manager;

import com.areeba.code.challenge.utils.logger.JLogger;
import org.hibernate.criterion.Order;

import java.util.List;

public class DBEntity<E> {

    private Class<E> entityClass = null;

    public DBEntity(Class<E> entityClass) {
        this.entityClass = entityClass;
    }

    public void dispose() {
        entityClass = null;
    }

    public void insert() {
        try {
            DBServices dbServices = new DBServices(getEntityClass());
            dbServices.insert(this);
            dbServices.dispose();
            dbServices = null;
        } catch (Exception e) {
            JLogger.getLogger().logException(e);
        }
    }

    public List<E> selectAll() {
        return selectAll(null);
    }

    public List<E> selectAll(Order orderBy) {
        try {
            DBServices dbServices = new DBServices(getEntityClass());
            List<E> eList = dbServices.selectAll(orderBy);
            dbServices.dispose();
            dbServices = null;
            return eList;
        } catch (Exception e) {
            JLogger.getLogger().logException(e);
        }
        return null;
    }

    public E find(String propertyId, Object propertyValue) {
        E entityRow = null;
        try {
            DBServices dbServices = new DBServices(getEntityClass());
            entityRow = (E) dbServices.find(propertyId, propertyValue);
            dbServices.dispose();
            dbServices = null;
        } catch (Exception e) {
            JLogger.getLogger().logException(e);
        }
        return entityRow;
    }

    public void remove(String propertyId, Object propertyValue) {
        try {
            DBServices dbServices = new DBServices(getEntityClass());
            dbServices.remove(propertyId, propertyValue);
            dbServices.dispose();
            dbServices = null;
        } catch (Exception e) {
            JLogger.getLogger().logException(e);
        }
    }

    private Class<E> getEntityClass() {
        return entityClass;
    }

    public String getOrderPropertyId() {
        return null;
    }
}

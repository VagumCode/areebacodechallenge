package com.areeba.code.challenge.service.actionLog;

import com.areeba.code.challenge.db.entities.Action;
import com.areeba.code.challenge.db.entities.ActionLog;
import com.areeba.code.challenge.db.entities.Device;
import com.areeba.code.challenge.utils.JUtilities;
import com.areeba.code.challenge.utils.logger.JLogger;
import org.hibernate.criterion.Order;

import java.util.Date;
import java.util.List;

public class ActionLogService {

    public void dispose() {
    }

    public void newActionLog(Device device, Action action, Date dateTime) {
        try {
            ActionLog actionLog = new ActionLog();
            actionLog.setDevice(device);
            actionLog.setAction(action);
            actionLog.setDate(dateTime);
            actionLog.insert();
        } catch (Exception e) {
            JLogger.getLogger().logException(e);
        }
    }

    public void removeActionLog(int actionLogId) {
        try {
            ActionLog actionLogEntity = new ActionLog();
            actionLogEntity.remove(ActionLog.FLD_ID, actionLogId);
            actionLogEntity.dispose();
            actionLogEntity = null;
        } catch (Exception e) {
            JLogger.getLogger().logException(e);
        }
    }

    public List<ActionLog> getActionsLogsList() {
        return getActionsLogsList(false);
    }

    public List<ActionLog> getActionsLogsList(boolean inAscendingOrder) {
        try {
            ActionLog actionLog = new ActionLog();
            Order orderBy = null;
            if (JUtilities.isStringValid(actionLog.getOrderPropertyId())) {
                orderBy = inAscendingOrder ? Order.asc(actionLog.getOrderPropertyId()) : Order.desc(actionLog.getOrderPropertyId());
            }
            List<ActionLog> actionsLogsList = actionLog.selectAll(orderBy);
            actionLog.dispose();
            actionLog = null;
            return actionsLogsList;
        } catch (Exception e) {
            JLogger.getLogger().logException(e);
        }
        return null;
    }
}

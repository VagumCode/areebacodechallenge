package com.areeba.code.challenge.servlets;

import com.areeba.code.challenge.utils.logger.JLogger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

public class WebServlet_General extends HttpServlet {

    public void sendOkResponse(HttpServletResponse resp, Object response) {
        sendResponse(resp, HttpServletResponse.SC_OK, response);
    }

    public void sendBadResponse(HttpServletResponse resp, Object response) {
        sendResponse(resp, HttpServletResponse.SC_BAD_REQUEST, response);
    }

    public void sendResponse(HttpServletResponse resp, int responseStatus, Object response) {
        try {
            if (resp != null) {
                resp.setContentType("application/json");
                resp.setCharacterEncoding("UTF-8");
                resp.setStatus(responseStatus);

                PrintWriter printWriter = resp.getWriter();
                printWriter.append(response + "");
                printWriter.flush();
                printWriter.close();
            }
        } catch (Exception e) {
            JLogger.getLogger().logException(e);
        }
    }
}

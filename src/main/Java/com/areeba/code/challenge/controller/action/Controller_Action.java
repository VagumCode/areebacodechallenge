package com.areeba.code.challenge.controller.action;

import com.areeba.code.challenge.config.AppConfiguration;
import com.areeba.code.challenge.db.entities.Action;
import com.areeba.code.challenge.utils.logger.JLogger;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/action")
public class Controller_Action extends Controller_ActionCommon {

    protected static final String KEY_ACTION_ID = "actionId";
    protected static final String KEY_ACTION_TITLE = "actionTitle";
    protected static final String KEY_SHOW_ALL = "showAll";

    @PostMapping(POST_MAP_ADD_NEW_ACTION)
    public @ResponseBody
    ModelAndView register(@RequestParam(KEY_ACTION_TITLE) String actionTitle) {
        ModelAndView modelAndView = new ModelAndView();
        try {
            Action action = getActionService().findActionByTitle(actionTitle);
            modelAndView.setViewName(AppConfiguration.PAGE_ACTIONS_TABLE);
            if (action == null) {
                getActionService().newAction(actionTitle);
                modelAndView.addObject(ATT_REGISTERED_ACTIONS_LIST, getActionService().getActionsList(false));
                modelAndView.setStatus(HttpStatus.OK);
            }
        } catch (Exception e) {
            JLogger.getLogger().logException(e);
        }
        return modelAndView;
    }

    @GetMapping(KEY_SHOW_ALL)
    public ModelAndView getDevicesList() {
        ModelAndView modelAndView = new ModelAndView();
        try {
            modelAndView.setViewName(AppConfiguration.PAGE_ACTIONS_TABLE);
            modelAndView.addObject(ATT_REGISTERED_ACTIONS_LIST, getActionService().getActionsList(false));
        } catch (Exception e) {
            JLogger.getLogger().logException(e);
        }
        return modelAndView;
    }

    @DeleteMapping("/delete/{" + KEY_ACTION_ID + "}")
    @ResponseStatus(HttpStatus.OK)
    public void removeDevice(@PathVariable int actionId) {
        try {
            getActionService().removeAction(actionId);
        } catch (Exception e) {
            JLogger.getLogger().logException(e);
        }
    }
}

package com.areeba.code.challenge.db.manager;

import com.areeba.code.challenge.db.entities.Action;
import com.areeba.code.challenge.db.entities.ActionLog;
import com.areeba.code.challenge.db.entities.Device;
import com.areeba.code.challenge.utils.logger.JLogger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.io.File;

public class DBConfiguration {

    private Configuration configuration = null;
    private static DBConfiguration dbConfiguration = null;

    public void dispose() {
        configuration = null;
        dbConfiguration = null;
    }

    private DBConfiguration() {
        init();
    }

    public static DBConfiguration getInstance() {
        if (dbConfiguration == null) {
            dbConfiguration = new DBConfiguration();
        }
        return dbConfiguration;
    }

    private void init() {
        registerEntities();
    }

    private void registerEntities() {
        try {
            getConfiguration().addAnnotatedClass(ActionLog.class);
            getConfiguration().addAnnotatedClass(Device.class);
            getConfiguration().addAnnotatedClass(Action.class);
        } catch (Exception e) {
            JLogger.getLogger().logException(e);
        }
    }

    private File getHibernateConfigFile() {
        try {
            return new File(getClass().getResource("/hibernate.cfg.xml").getPath());
        } catch (Exception e) {
            JLogger.getLogger().logException(e);
        }
        return null;
    }

    private Configuration getConfiguration() {
        if (configuration == null) {
            configuration = new Configuration().configure(getHibernateConfigFile());
        }
        return configuration;
    }

    public Session openSession() {
        Session session = null;
        try {
            SessionFactory sessionFactory = getConfiguration().buildSessionFactory();
            session = sessionFactory.openSession();
        } catch (Exception e) {
            JLogger.getLogger().logException(e);
        }
        return session;
    }
}

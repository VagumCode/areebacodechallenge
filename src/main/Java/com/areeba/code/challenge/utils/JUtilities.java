package com.areeba.code.challenge.utils;

public class JUtilities {

    public static String getValidString(String value1, String value2) {
        return isStringValid(value1) ? value1 : value2;
    }

    public static boolean isStringValid(String strg) {
        return strg != null && !strg.isEmpty() && !strg.equalsIgnoreCase("null") && !strg.equalsIgnoreCase("undefined");
    }

    public static String toEmptyStringIfNotValid(String value) {
        String emptyString = value;
        try {
            if (!isStringValid(value)) {
                emptyString = "";
            }
        } catch (Exception e) {
            emptyString = "";
        }
        return emptyString;
    }

    public static boolean toBoolean(String value) {
        boolean booleanValue = false;
        try {
            booleanValue = toBoolean(value, false);
        } catch (Exception e) { }
        return booleanValue;
    }

    public static boolean toBoolean(String value, boolean defaultValue) {
        boolean booleanValue = defaultValue;
        try {
            if (isStringValid(value) && (value.equalsIgnoreCase("true") || value.equals("1") || value.equalsIgnoreCase("on") || value.equalsIgnoreCase("yes"))) {
                booleanValue = true;
            } else {
                booleanValue = false;
            }
        } catch (Exception e) { }
        return booleanValue;
    }

    public static boolean isNumber(Object value) {
        boolean isNumber = true;
        try {
            isNumber = isInteger(value) || isDouble(value) || isFloat(value) || isLong(value);
        } catch (Exception e) {
            isNumber = false;
        }
        return isNumber;
    }

    public static boolean isBoolean(Object value) {
        boolean isBoolean = true;
        try {
            Boolean.parseBoolean(value + "");
        } catch (Exception e) {
            isBoolean = false;
        }
        return isBoolean;
    }

    public static boolean isInteger(Object value) {
        boolean isInteger = true;
        try {
            Integer.parseInt(value + "");
        } catch (Exception e) {
            isInteger = false;
        }
        return isInteger;
    }

    public static boolean isLong(Object value) {
        boolean isLong = true;
        try {
            Long.parseLong(value + "");
        } catch (Exception e) {
            isLong = false;
        }
        return isLong;
    }

    public static boolean isFloat(Object value) {
        boolean isFloat = true;
        try {
            Float.parseFloat(value + "");
        } catch (Exception e) {
            isFloat = false;
        }
        return isFloat;
    }

    public static boolean isDouble(Object value) {
        boolean isDouble = true;
        try {
            Double.parseDouble(value + "");
        } catch (Exception e) {
            isDouble = false;
        }
        return isDouble;
    }

    public static int toInt(String value) {
        return toInt(value, 0);
    }

    public static int toInt(String value, int defaultValue) {
        int integerValue = defaultValue;
        try {
            integerValue = Integer.valueOf(value);
        } catch (Exception e) {}
        return integerValue;
    }

    public static long toLong(String value) {
        return toInt(value, 0);
    }

    public static long toLong(String value, int defaultValue) {
        long longValue = defaultValue;
        try {
            longValue = Long.valueOf(value);
        } catch (Exception e) {}
        return longValue;
    }

    public static double toDouble(String value) {
        return toDouble(value, 0);
    }

    public static double toDouble(String value, double defaultValue) {
        double integerValue = defaultValue;
        try {
            integerValue = Double.valueOf(value);
        } catch (Exception e) {}
        return integerValue;
    }
}

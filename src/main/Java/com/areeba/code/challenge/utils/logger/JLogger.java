package com.areeba.code.challenge.utils.logger;

import com.areeba.code.challenge.utils.DateUtilities;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.StringWriter;

public class JLogger {

    private static final String LOG_LEVEL_ALL       = "ALL";
    private static final String LOG_LEVEL_INFO      = "INFO";
    private static final String LOG_LEVEL_ERROR     = "ERROR";
    private static final String LOG_LEVEL_EXCEPTION = "EXCEPTION";

    private static final String LOG_LEVEL = LOG_LEVEL_ALL;

    private File infoLogFile     = null;

    private static JLogger logger = null;

    public static JLogger getLogger(){
        if(logger == null){
            logger = new JLogger();
        }
        return logger;
    }

    public void dispose(){
        infoLogFile = null;
    }

    public void logLocationInfo() {
        writeIntoLogFile(LOG_LEVEL_INFO, "", null, false);
    }

    public void logLocationInfo(Object log) {
        writeIntoLogFile(LOG_LEVEL_INFO, log + "", null, false);
    }

    public void logInfo(Object message){
        writeIntoLogFile(LOG_LEVEL_INFO, message, null, false);
    }

    public void logError(String message){
        writeIntoLogFile(LOG_LEVEL_ERROR, message, null, true);
    }

    public void logException(Exception exception){
        logException(exception, null);
    }

    public void logException(Exception exception, String errorMessage){
        StringWriter errors = new StringWriter();
        if(exception != null) {
            exception.printStackTrace(new PrintWriter(errors));
        }
        writeIntoLogFile(LOG_LEVEL_EXCEPTION, errors.toString(), errorMessage, true);
    }

    public void logExceptionInfo(String errorMessage){
        StringWriter errors = new StringWriter();
        writeIntoLogFile(LOG_LEVEL_EXCEPTION, errors.toString(), errorMessage, true);
    }

    private void writeIntoLogFile(String logFileLevel, Object log, String errorMessage, boolean isErrorLog){
        try {
            if (LOG_LEVEL.equalsIgnoreCase(logFileLevel) || LOG_LEVEL.equalsIgnoreCase(LOG_LEVEL_ALL)) {
                StringBuilder builder = new StringBuilder();

                Thread currentThread = Thread.currentThread();
                if (currentThread != null) {
                    StackTraceElement stackTraceElement = currentThread.getStackTrace().length > 3 ? currentThread.getStackTrace()[3] : null;
                    if (stackTraceElement != null) {
                        builder.append("[");
                        builder.append(getCurrentDateTime());
                        builder.append("][" + logFileLevel + "]");
                        builder.append("[DB: MySQL]");
                        builder.append("[" + stackTraceElement.getFileName() + "]");
                        builder.append("[" + stackTraceElement.getMethodName() + "]");
                        builder.append("[" + stackTraceElement.getLineNumber() + "]");
                        if(errorMessage != null && !errorMessage.isEmpty()){
                            builder.append("[");
                            builder.append(errorMessage);
                            builder.append("]");
                        }
                        builder.append("[");
                        builder.append(log);
                        builder.append("]");

                        builder.append("\n");
                    }

                    if (getLogFile() != null) {
                        FileWriter fileWriter = new FileWriter(getLogFile(), true);
                        fileWriter.write(builder.toString());
                        fileWriter.flush();
                        fileWriter.close();
                    } else {
                        if (isErrorLog) {
                            System.err.println(builder.toString());
                        } else {
                            System.out.println(builder.toString());
                        }
                    }
                }
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    private File getLogFile(){
        if(getLogFileName() != null && !getLogFileName().isEmpty()){
            if(infoLogFile == null){
                infoLogFile = new File(getLogFileName());
                try{
                    if(!infoLogFile.exists()){
                        infoLogFile.createNewFile();
                    }
                }catch(Exception e){
                    System.err.println(e.getMessage());
                }
            }
        }
        return infoLogFile;
    }

    private String getLogFileName() {
        String filePath = getFilePath();
        File logFile = new File(filePath);
        if (!logFile.exists()) {
            logFile.mkdirs();
        }
        return filePath.concat(File.separator).concat(getFileName());
    }

    private String getFilePath() {
        return "/Users/user/Documents/VagumCode/doc/log";
    }

    protected String getFileName() {
        return getFilePrefixName().concat("-").concat(getCurrentDate().replaceAll(" ", "-") + ".log");
    }

    protected String getFilePrefixName() {
        return "areeba-code-challenge";
    }

    private String getCurrentDateTime(){
        return DateUtilities.getLebaneseDateTime();
    }

    private String getCurrentDate(){
        return DateUtilities.getLebaneseDate();
    }
}
